Edit /etc/consul/xivo/config.json :
    "bind_addr": "0.0.0.0",
    "client_addr": "0.0.0.0",

Then:
rabbitmqctl add_user xivomds xivomds
rabbitmqctl set_user_tags xivomds administrator
rabbitmqctl set_permissions -p / xivomds ".*" ".*" ".*"

Then:
monit unmonitor xivo-auth
rm (or comment out) /usr/share/xivo-monitoring/checks/xivo-auth
systemctl stop xivo-auth.service
systemctl disable xivo-auth.service

Then edit /usr/bin/xivo-service :
sed -i 's/\(xivo_services=.*\)xivo-auth /\1/' /usr/bin/xivo-service
docker_services="config_mgt xivo-auth"


mkdir /etc/xivo/mds
cat > /etc/xivo/mds/010-mds_auth.yml << EOF
auth:
  host: 172.17.0.1
  verify_certificate: False
EOF

ln -s "/etc/xivo/mds/010-mds_auth.yml" "/etc/xivo-agentd/conf.d/010-mds_auth.yml"
ln -s "/etc/xivo/mds/010-mds_auth.yml" "/etc/xivo-agid/conf.d/010-mds_auth.yml"
ln -s "/etc/xivo/mds/010-mds_auth.yml" "/etc/xivo-amid/conf.d/010-mds_auth.yml"
ln -s "/etc/xivo/mds/010-mds_auth.yml" "/etc/xivo-confd/conf.d/010-mds_auth.yml"
ln -s "/etc/xivo/mds/010-mds_auth.yml" "/etc/xivo-ctid/conf.d/010-mds_auth.yml"
ln -s "/etc/xivo/mds/010-mds_auth.yml" "/etc/xivo-dird/conf.d/010-mds_auth.yml"
ln -s "/etc/xivo/mds/010-mds_auth.yml" "/etc/xivo-dird-phoned/conf.d/010-mds_auth.yml"

