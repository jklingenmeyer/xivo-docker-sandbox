#FROM python:2.7-slim-jessie
FROM python:2.7-jessie
MAINTAINER XiVO Team "dev@avencall.com"

RUN apt-get -yq update \
   && apt-get -yqq dist-upgrade \
   && apt-get -yq autoremove \
   && apt-get install libldap2-dev \
                      libsasl2-dev

# Install
ADD . /usr/src/xivo-auth
WORKDIR /usr/src/xivo-auth
RUN pip install --upgrade pip
#RUN pip install setuptools==39.2.0
RUN pip install -r requirements.txt
RUN python setup.py install

#Configure environment
RUN touch /var/log/xivo-auth.log
RUN mkdir -p /etc/xivo-auth/conf.d
RUN cp /usr/src/xivo-auth/etc/xivo-auth/*.yml /etc/xivo-auth/
RUN install -d -o www-data -g www-data /var/run/xivo-auth/

# Configure Service
RUN mkdir -p /etc/xivo/mds
RUN echo 'db_uri: postgresql://asterisk:proformatique@XIVO_HOST/asterisk' > /etc/xivo/mds/010-mds_db_uri.yml
RUN echo 'listen_address: 0.0.0.0\nlisten_port: 4573' > /etc/xivo/mds/010-mds_listen_address.yml
RUN echo " \n\
  consul: \n\
    host: CONSUL_HOST \n\
    verify: False \n\
" > /etc/xivo/mds/020-mds_consul.yml
RUN echo " \n\
amqp: \n\
  uri: amqp://xivomds:xivomds@AMQP_HOST:5672/ \n\
" > /etc/xivo/mds/030-mds_amqp.yml

#xivo-auth
RUN ln -s "/etc/xivo/mds/010-mds_db_uri.yml" "/etc/xivo-auth/conf.d/010-mds_db_uri.yml"
RUN ln -s "/etc/xivo/mds/010-mds_listen_address.yml" "/etc/xivo-auth/conf.d/010-mds_listen_address.yml"
RUN ln -s "/etc/xivo/mds/020-mds_consul.yml" "/etc/xivo-auth/conf.d/020-mds_consul.yml"
RUN ln -s "/etc/xivo/mds/030-mds_amqp.yml" "/etc/xivo-auth/conf.d/030-mds_amqp.yml"

#xivo-update-keys (/var/lib/xivo-auth-keys/xivo-****-key.yml)

ADD ./contribs/docker/certs /usr/share/xivo-certs

# Clean
RUN apt-get clean
RUN rm -rf /usr/src/xivo-auth

EXPOSE 9497

CMD ["xivo-auth", "-fd", "--user", "www-data", "-c", "/etc/xivo-auth/config.yml"]
